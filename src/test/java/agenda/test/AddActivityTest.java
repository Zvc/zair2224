package agenda.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.interfaces.RepositoryActivity;

import org.junit.Before;
import org.junit.Test;

public class AddActivityTest {
	private Activity act;
	private Contact contact;
	private RepositoryActivity rep;
	private LinkedList<Contact> list;
	
	@Before
	public void setUp() throws Exception {
		list = new LinkedList<Contact>();
		contact = new Contact("Name1","asd","072822");
		list.add(contact);
		rep = new RepositoryActivityMock();
	}
	
	@Test
	public void testCase1()
	{

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name1", 
					df.parse("03/20/2013 12:00"), 
					df.parse("03/20/2013 13:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(1 == rep.count());
	}
	
	@Test
	public void testCase2()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{

			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("name1",
					df.parse("03/20/2013 12:00"), 
					df.parse("03/20/2013 13:00"),
					null,
					"Lunch break");
			assertFalse(rep.addActivity(act));
		}
		catch(Exception e){}	
		int c = rep.count();
		assertTrue( c == 0);

	}

	@Test
	public void testCase3()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{

			act = new Activity("name2",
					df.parse("03/20/2013 14:00"),
					df.parse("03/20/2013 15:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

			act = new Activity("name1",
					df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"),
					null,
					"Lunch break");
			assertFalse(rep.addActivity(act));
		}
		catch(Exception e){}
		int c = rep.count();
		assertTrue( c == 1);

	}
	
	@Test
	public void testCase4()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{

			for (Activity a : rep.getActivities())
				rep.removeActivity(a);
			
			act = new Activity("name1",
					df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

			act = new Activity("name2",
					df.parse("03/20/2013 12:30"),
					df.parse("03/20/2013 13:30"),
					list,
					"Lunch break");
			assertFalse(rep.addActivity(act));
		}
		catch(Exception e){}	
		assertTrue( 1 == rep.count());

	}

	@Test
	public void testCase5()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{

			act = new Activity("name1",
					df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

			act = new Activity("name2",
					df.parse("03/20/2013 20:00"),
					df.parse("03/20/2013 21:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

			act = new Activity("name3",
					df.parse("03/20/2013 13:00"),
					df.parse("03/20/2013 13:30"),
					list,
					"Work");
			rep.addActivity(act);
		}
		catch(Exception e){}
		int c = rep.count();
		assertTrue( c == 3);

	}

	@Test
	public void testCase6()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{

			for(int i=0; i<=4; i++)
			{
				act = new Activity("name",
						df.parse("03/20/2013 12:" + i + "" + i),
						df.parse("03/20/2013 12:" + i + "" + (i+1)),
						list,
						"Lunch break");
				rep.addActivity(act);
			}

			act = new Activity("name1",
					df.parse("03/20/2013 12:55"),
					df.parse("03/20/2013 12:56"),
					list,
					"Lunch break");
			rep.addActivity(act);
		}
		catch(Exception e){}
		int c = rep.count();
		assertTrue( c == 6);
	}


	@Test
	public void testCase7() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {

			for (Activity a : rep.getActivities())
				rep.removeActivity(a);

			act = new Activity("name1",
					df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"),
					list,
					"Lunch break");
			rep.addActivity(act);

			act = new Activity("name2",
					df.parse("03/20/2013 11:50"),
					df.parse("03/20/2013 12:10"),
					list,
					"Lunch break");
			assertFalse(rep.addActivity(act));
		} catch (Exception e) {
		}
		assertTrue(1 == rep.count());
	}
}
