package agenda.test;

import static org.junit.Assert.*;

import agenda.startApp.MainClass;
import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;

	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			con = new Contact("Alexandra", "adresa", "07456734");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}


	@Test
	public void testCase2()
	{
		try {
			con = new Contact("Alexandra M V", "adresa", "07456734");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		rep.addContact(con);
		if(rep.count() == 0)
			assertTrue(true);

	}

	@Test
	public void testCase3()
	{
		try {
			con = new Contact("George", "adresa", "+1234");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
	}
	@Test
	public void testCase4()
	{
		try {
			con = new Contact("V", "adresa", "489");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		rep.addContact(con);
		if(rep.count() == 0)
			assertTrue(true);
	}

	@Test
	public void testCase5()
	{
		try {
			con = new Contact("B B B", "adresa", "+489");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
		rep.addContact(con);
		if(rep.count() == 0)
			assertTrue(true);
	}

	@Test
	public void testCase6()
	{
		try {
			con = new Contact("", "adresa", "05");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		rep.addContact(con);
		if(rep.count() == 1)
			assertTrue(true);
	}
	
	@Test
	public void testCase7()
	{
		try {
			con = new Contact("Ana", "adresa", "");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		catch (Exception e){
			assertTrue(true);
		}

		rep.addContact(con);
		if(rep.count() == 0)
			assertTrue(true);
	}

	@Test
	public void testCase8()
	{
		try {
			con = new Contact("Ana", "adresa", "0");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}

		rep.addContact(con);
		if(rep.count() == 1)
			assertTrue(true);
	}
	
	@Test
	public void testCase9() {
		try {
			con = new Contact("Ana Maria", "adresa", "039");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		rep.addContact(con);
		for (Contact c : rep.getContacts())
			if (c.equals(con)) {
				assertTrue(true);
				break;
			}
	}

	@Test
	public void testCase10() {
		try {
			con = new Contact("Ana Maria", "adresa", "+");
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

		rep.addContact(con);
		if(rep.count() == 0)
			assertTrue(true);
	}
	
}
