package agenda.controller.service;

import agenda.model.base.User;
import agenda.model.repository.interfaces.RepositoryUser;

public class UserService {

    RepositoryUser repositoryUser;

    public UserService(RepositoryUser repositoryUser) {
        this.repositoryUser = repositoryUser;
    }

    public void addUser(User user)
    {

    }

    public void changePassword(String newPassword){}

    public void generateContract(User user){}

    public User getByUsername(String username)
    {
        return repositoryUser.getByUsername(username);
    }
}
