package agenda.model.validator;

import java.util.List;

public interface Validator<E> {

    public void validate(E e, List<String> list);
}
